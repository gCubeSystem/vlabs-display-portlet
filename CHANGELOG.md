
# Changelog for SBD Upload and Share Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.1.0] - 2023-07-21

- Added possibility to choose icon URL per item / vlab and its width

## [v1.0.0] - 2023-03-08

First Release
