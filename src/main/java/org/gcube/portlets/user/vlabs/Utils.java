package org.gcube.portlets.user.vlabs;

import org.gcube.common.portal.GCubePortalConstants;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GroupMembershipType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.GroupLocalServiceUtil;

public class Utils {
	private static Log _log = LogFactoryUtil.getLog(Utils.class);
	/**
	 * 
	 * @param currentUser
	 * @param urlContainingGroupId
	 * @return the access policy and the direct VRE Url if the member belongs to the VRE, the page to ask for registration on this VRE othwerise
	 */
	public static VLabBean getVLabBean(User currentUser, String urlContainingGroupId) {
		VLabBean toReturn = new VLabBean();
		toReturn.setFriendlyURL("");
		toReturn.setAccessPolicy(GroupMembershipType.PRIVATE);
		String[] splits = urlContainingGroupId.split("=");
		long groupId = -1;
		try {
			toReturn.setFriendlyURL(urlContainingGroupId);
			if (splits.length > 0) {
				groupId = Long.parseLong(splits[1]);
				GroupManager gm = new LiferayGroupManager();
				GCubeGroup theGroup = gm.getGroup(groupId);
				GroupMembershipType ap =theGroup.getMembershipType();
				System.out.println("Access Policy for " + theGroup.getGroupName() + ": " + ap.name());
				toReturn.setAccessPolicy(ap);
				long[] userGroupIds = currentUser.getGroupIds();
				for (int i = 0; i < userGroupIds.length; i++) {
					if (groupId == userGroupIds[i]) {
						String privatePagesURL = GCubePortalConstants.PREFIX_GROUP_URL +GroupLocalServiceUtil.getGroup(groupId).getFriendlyURL();
						toReturn.setFriendlyURL(privatePagesURL);
					}
				}
			} 
		} catch (Exception e) {
			_log.error("Something is wrong in the url passed: " + urlContainingGroupId);
			return toReturn;
		}
		return toReturn;
	}
}
