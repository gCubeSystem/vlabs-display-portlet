package org.gcube.portlets.user.vlabs;

import org.gcube.vomanagement.usermanagement.model.GroupMembershipType;

public class VLabBean {
	
	private GroupMembershipType accessPolicy;
	private String friendlyURL;
	
	/**
	 * 
	 */
	public VLabBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param accessPolicy
	 * @param friendlyURL
	 */
	public VLabBean(GroupMembershipType accessPolicy, String friendlyURL) {
		super();
		this.accessPolicy = accessPolicy;
		this.friendlyURL = friendlyURL;
	}
	public GroupMembershipType getAccessPolicy() {
		return accessPolicy;
	}
	public void setAccessPolicy(GroupMembershipType accessPolicy) {
		this.accessPolicy = accessPolicy;
	}
	public String getFriendlyURL() {
		return friendlyURL;
	}
	public void setFriendlyURL(String friendlyURL) {
		this.friendlyURL = friendlyURL;
	}
	@Override
	public String toString() {
		return "VLabBean [accessPolicy=" + accessPolicy + ", friendlyURL=" + friendlyURL + "]";
	}
	
	

}
