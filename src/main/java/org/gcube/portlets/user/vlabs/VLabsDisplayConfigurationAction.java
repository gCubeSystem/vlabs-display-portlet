package org.gcube.portlets.user.vlabs;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

public class VLabsDisplayConfigurationAction extends DefaultConfigurationAction {
	private static Log _log = LogFactoryUtil.getLog(VLabsDisplayConfigurationAction.class);
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {      	  
		super.processAction(portletConfig, actionRequest, actionResponse);
		PortletPreferences prefs = actionRequest.getPreferences();
		String icondocURL = prefs.getValue("icondocURL", "0");     
		_log.info("icondocURL = " + icondocURL + " in ConfigurationAction.processAction() saved correctly");
		String icondocURLWidthPx = prefs.getValue("icondocURLWidthPx", "0");     
		_log.info("icondocURLWidthPx = " + icondocURLWidthPx + " in ConfigurationAction.processAction() saved correctly");
		
		String vlabName1Icon = prefs.getValue("vlabName1-Icon", "");     
		_log.info("vlabName1-Icon = " + vlabName1Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName2Icon = prefs.getValue("vlabName2-Icon", "");     
		_log.info("vlabName2-Icon = " + vlabName2Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName3Icon = prefs.getValue("vlabName3-Icon", "");     
		_log.info("vlabName3-Icon = " + vlabName3Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName4Icon = prefs.getValue("vlabName4-Icon", "");     
		_log.info("vlabName4-Icon = " + vlabName4Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName5Icon = prefs.getValue("vlabName5-Icon", "");     
		_log.info("vlabName5-Icon = " + vlabName5Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName6Icon = prefs.getValue("vlabName6-Icon", "");     
		_log.info("vlabName6-Icon = " + vlabName6Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName7Icon = prefs.getValue("vlabName7-Icon", "");     
		_log.info("vlabName7-Icon = " + vlabName7Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName8Icon = prefs.getValue("vlabName8-Icon", "");     
		_log.info("vlabName8-Icon = " + vlabName8Icon + " in ConfigurationAction.processAction() saved correctly");
		String vlabName9Icon = prefs.getValue("vlabName9-Icon", "");     
		_log.info("vlabName9-Icon = " + vlabName9Icon + " in ConfigurationAction.processAction() saved correctly");
		
		

		String vlabName1Title = prefs.getValue("vlabName1-Title", "");     
		_log.info("vlabName1-Title = " + vlabName1Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName2Title = prefs.getValue("vlabName2-Title", "");     
		_log.info("vlabName2-Title = " + vlabName2Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName3Title = prefs.getValue("vlabName3-Title", "");     
		_log.info("vlabName3-Title = " + vlabName3Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName4Title = prefs.getValue("vlabName4-Title", "");     
		_log.info("vlabName4-Title = " + vlabName4Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName5Title = prefs.getValue("vlabName5-Title", "");     
		_log.info("vlabName5-Title = " + vlabName5Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName6Title = prefs.getValue("vlabName6-Title", "");     
		_log.info("vlabName6-Title = " + vlabName6Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName7Title = prefs.getValue("vlabName7-Title", "");     
		_log.info("vlabName7-Title = " + vlabName7Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName8Title = prefs.getValue("vlabName8-Title", "");     
		_log.info("vlabName8-Title = " + vlabName8Title + " in ConfigurationAction.processAction() saved correctly");
		String vlabName9Title = prefs.getValue("vlabName9-Title", "");     
		_log.info("vlabName9-Title = " + vlabName9Title + " in ConfigurationAction.processAction() saved correctly");

		
		long vlabName1DocumentId =  Long.parseLong(prefs.getValue("vlabName1-DocumentId", "0"));     
		_log.info("vlabName1-DocumentId = " + vlabName1DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName2DocumentId =  Long.parseLong(prefs.getValue("vlabName2-DocumentId", "0"));     
		_log.info("vlabName2-DocumentId = " + vlabName2DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName3DocumentId =  Long.parseLong(prefs.getValue("vlabName3-DocumentId", "0"));     
		_log.info("vlabName3-DocumentId = " + vlabName3DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName4DocumentId =  Long.parseLong(prefs.getValue("vlabName4-DocumentId", "0"));     
		_log.info("vlabName4-DocumentId = " + vlabName4DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName5DocumentId =  Long.parseLong(prefs.getValue("vlabName5-DocumentId", "0"));     
		_log.info("vlabName5-DocumentId = " + vlabName5DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName6DocumentId =  Long.parseLong(prefs.getValue("vlabName6-DocumentId", "0"));     
		_log.info("vlabName6-DocumentId = " + vlabName6DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName7DocumentId =  Long.parseLong(prefs.getValue("vlabName7-DocumentId", "0"));     
		_log.info("vlabName7-DocumentId = " + vlabName7DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName8DocumentId =  Long.parseLong(prefs.getValue("vlabName8-DocumentId", "0"));     
		_log.info("vlabName8-DocumentId = " + vlabName8DocumentId + " in ConfigurationAction.processAction() saved correctly");
		long vlabName9DocumentId =  Long.parseLong(prefs.getValue("vlabName9-DocumentId", "0"));     
		_log.info("vlabName9-DocumentId = " + vlabName9DocumentId + " in ConfigurationAction.processAction() saved correctly");

		
		String vlab1Url = prefs.getValue("vlab1-Url", "");     
		_log.info("vlab1-Url = " + vlab1Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab2Url = prefs.getValue("vlab2-Url", "");     
		_log.info("vlab2-Url = " + vlab2Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab3Url = prefs.getValue("vlab3-Url", "");     
		_log.info("vlab3-Url = " + vlab3Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab4Url = prefs.getValue("vlab4-Url", "");     
		_log.info("vlab4-Url = " + vlab4Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab5Url = prefs.getValue("vlab5-Url", "");     
		_log.info("vlab5-Url = " + vlab5Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab6Url = prefs.getValue("vlab6-Url", "");     
		_log.info("vlab6-Url = " + vlab6Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab7Url = prefs.getValue("vlab7-Url", "");     
		_log.info("vlab7-Url = " + vlab7Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab8Url = prefs.getValue("vlab8-Url", "");     
		_log.info("vlab8-Url = " + vlab8Url + " in ConfigurationAction.processAction() saved correctly");
		String vlab9Url = prefs.getValue("vlab9-Url", "");     
		_log.info("vlab9-Url = " + vlab9Url + " in ConfigurationAction.processAction() saved correctly");
	

	}

	@Override
	public String render(PortletConfig portletConfig,
			RenderRequest renderRequest, RenderResponse renderResponse)
					throws Exception {
		return "/html/vlabsdisplay/config.jsp";
	}

}