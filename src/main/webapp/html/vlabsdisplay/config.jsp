<%@include file="init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

Here you can customise the icon to show besides the vlab name

<%
	String icondocURL = GetterUtil.getString(portletPreferences.getValue("icondocURL", StringPool.BLANK));
	String icondocURLWidthPx = GetterUtil.getString(portletPreferences.getValue("icondocURLWidthPx", StringPool.BLANK));

	String vlabName1Icon = GetterUtil.getString(portletPreferences.getValue("vlabName1-Icon", StringPool.BLANK));
	String vlabName2Icon = GetterUtil.getString(portletPreferences.getValue("vlabName2-Icon", StringPool.BLANK));
	String vlabName3Icon = GetterUtil.getString(portletPreferences.getValue("vlabName3-Icon", StringPool.BLANK));
	String vlabName4Icon = GetterUtil.getString(portletPreferences.getValue("vlabName4-Icon", StringPool.BLANK));
	String vlabName5Icon = GetterUtil.getString(portletPreferences.getValue("vlabName5-Icon", StringPool.BLANK));
	String vlabName6Icon = GetterUtil.getString(portletPreferences.getValue("vlabName6-Icon", StringPool.BLANK));
	String vlabName7Icon = GetterUtil.getString(portletPreferences.getValue("vlabName7-Icon", StringPool.BLANK));
	String vlabName8Icon = GetterUtil.getString(portletPreferences.getValue("vlabName8-Icon", StringPool.BLANK));
	String vlabName9Icon = GetterUtil.getString(portletPreferences.getValue("vlabName9-Icon", StringPool.BLANK));


	String vlabName1Title = GetterUtil.getString(portletPreferences.getValue("vlabName1-Title", StringPool.BLANK));
	String vlabName2Title = GetterUtil.getString(portletPreferences.getValue("vlabName2-Title", StringPool.BLANK));
	String vlabName3Title = GetterUtil.getString(portletPreferences.getValue("vlabName3-Title", StringPool.BLANK));
	String vlabName4Title = GetterUtil.getString(portletPreferences.getValue("vlabName4-Title", StringPool.BLANK));
	String vlabName5Title = GetterUtil.getString(portletPreferences.getValue("vlabName5-Title", StringPool.BLANK));
	String vlabName6Title = GetterUtil.getString(portletPreferences.getValue("vlabName6-Title", StringPool.BLANK));
	String vlabName7Title = GetterUtil.getString(portletPreferences.getValue("vlabName7-Title", StringPool.BLANK));
	String vlabName8Title = GetterUtil.getString(portletPreferences.getValue("vlabName8-Title", StringPool.BLANK));
	String vlabName9Title = GetterUtil.getString(portletPreferences.getValue("vlabName9-Title", StringPool.BLANK));
	

	long vlabName1DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName1-DocumentId", StringPool.BLANK));
	long vlabName2DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName2-DocumentId", StringPool.BLANK));
	long vlabName3DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName3-DocumentId", StringPool.BLANK));
	long vlabName4DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName4-DocumentId", StringPool.BLANK));
	long vlabName5DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName5-DocumentId", StringPool.BLANK));
	long vlabName6DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName6-DocumentId", StringPool.BLANK));
	long vlabName7DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName7-DocumentId", StringPool.BLANK));
	long vlabName8DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName8-DocumentId", StringPool.BLANK));
	long vlabName9DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName9-DocumentId", StringPool.BLANK));
	

	String vlab1Url = GetterUtil.getString(portletPreferences.getValue("vlab1-Url", StringPool.BLANK));
	String vlab2Url = GetterUtil.getString(portletPreferences.getValue("vlab2-Url", StringPool.BLANK));
	String vlab3Url = GetterUtil.getString(portletPreferences.getValue("vlab3-Url", StringPool.BLANK));
	String vlab4Url = GetterUtil.getString(portletPreferences.getValue("vlab4-Url", StringPool.BLANK));
	String vlab5Url = GetterUtil.getString(portletPreferences.getValue("vlab5-Url", StringPool.BLANK));
	String vlab6Url = GetterUtil.getString(portletPreferences.getValue("vlab6-Url", StringPool.BLANK));
	String vlab7Url = GetterUtil.getString(portletPreferences.getValue("vlab7-Url", StringPool.BLANK));
	String vlab8Url = GetterUtil.getString(portletPreferences.getValue("vlab8-Url", StringPool.BLANK));
	String vlab9Url = GetterUtil.getString(portletPreferences.getValue("vlab9-Url", StringPool.BLANK));
	

	
	String displayName_cfg = "";
%>

<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />
	<aui:input name="preferences--icondocURL--" type="text"
		cssClass="text long-field" showRequiredLabel="true" label="Icon docId"
		inlineField="true" inlineLabel="left"
		placeholder="Relative URL of the image"
		helpMessage="Relative URL of the image taken from Liferay CMS of the icons"
		value="<%=icondocURL%>" required="false">
	</aui:input>
		
	<aui:input name="preferences--icondocURLWidthPx--" type="text"
		cssClass="text long-field" showRequiredLabel="true" label="Icon size in px (default 32px)"
		inlineField="true" inlineLabel="left"
		placeholder="Width in px of the image"
		helpMessage="width in pixel of the icon"
		value="<%=icondocURLWidthPx%>" required="false">
	</aui:input>
	
	<p>Enter the vlab in the following:</p>
	<!-- vlab URL -->
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
				<td><aui:input name="preferences--vlabName1-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 1 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName1Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName1-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 1" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName1Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName1-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 1" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName1DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab1-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 1" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab1Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName2-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 2 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName2Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName2-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 2" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName2Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName2-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 2" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName2DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab2-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 2" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab2Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName3-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 3 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName3Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName3-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 3" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName3Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName3-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 3" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName3DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab3-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 3" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab3Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName4-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 4 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName4Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName4-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 4" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName4Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName4-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 4" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName4DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab4-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 4" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab4Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName5-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 5 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName5Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName5-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 5" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName5Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName5-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 5" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName5DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab5-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 5" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab5Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName6-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 6 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName6Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName6-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 6" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName6Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName6-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 6" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName6DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab6-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 6" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab6Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName7-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 7 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName7Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName7-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 7" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName7Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName7-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 7" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName7DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab7-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 7" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab7Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName8-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 8 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName8Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName8-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 8" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName8Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName8-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 8" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName8DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab8-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 8" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab8Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:field-wrapper cssClass="field-group">
		<table>
			<tr>
			<td><aui:input name="preferences--vlabName9-Icon--"
						type="text" cssClass="text long-field" showRequiredLabel="false"
						label="vlab Name 9 Icon URL" inlineField="true" inlineLabel="left"
						placeholder="vlab icon URL" helpMessage="The icon URL of the vlab, leave empty if want to apply the same icon URL above for every item"
						value="<%=vlabName9Icon%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName9-Title--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Name 9" inlineField="true" inlineLabel="left"
						placeholder="vlab" helpMessage="Display Name of the vlab"
						value="<%=vlabName9Title%>" required="false" /></td>
				<td><aui:input name="preferences--vlabName9-DocumentId--"
						type="text" cssClass="text long-field" showRequiredLabel="true"
						label="vlab Document Id 9" inlineField="true" inlineLabel="left"
						placeholder="ID"
						helpMessage="ID taken from Liferay CMS in this Site"
						value="<%=vlabName9DocumentId%>" required="false">
						<aui:validator name="digits"></aui:validator>
					</aui:input></td>
				<td><aui:input name="preferences--vlab9-Url--" type="text"
						cssClass="text long-field" showRequiredLabel="true"
						label="URL of the VRE 9" inlineField="true" inlineLabel="left"
						placeholder="url" helpMessage="URL" value="<%=vlab9Url%>"
						required="false">
					</aui:input></td>
			</tr>
		</table>
	</aui:field-wrapper>
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>