<%@include file="init.jsp"%>
<%@ page import="org.gcube.portlets.user.vlabs.Utils" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>

<%
	final int abstractCharsLenghtMax = 256;

	long groupId = themeDisplay.getLayout().getGroupId();
	User currentUser = PortalUtil.getUser( request );
	
	String icondocURL = GetterUtil.getString(portletPreferences.getValue("icondocURL", StringPool.BLANK));
	pageContext.setAttribute("icondocURL",icondocURL);
		
	String icondocURLWidthPx = GetterUtil.getString(portletPreferences.getValue("icondocURLWidthPx", StringPool.BLANK));
	if (icondocURLWidthPx == null || icondocURLWidthPx.compareTo(StringPool.BLANK) == 0) {
		icondocURLWidthPx = "32";
	}		
	pageContext.setAttribute("icondocURLWidthPx",icondocURLWidthPx);
	
	String vlabName1Icon = GetterUtil.getString(portletPreferences.getValue("vlabName1-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName1Icon",vlabName1Icon);
	String vlabName2Icon = GetterUtil.getString(portletPreferences.getValue("vlabName2-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName2Icon",vlabName2Icon);
	String vlabName3Icon = GetterUtil.getString(portletPreferences.getValue("vlabName3-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName3Icon",vlabName3Icon);
	String vlabName4Icon = GetterUtil.getString(portletPreferences.getValue("vlabName4-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName4Icon",vlabName4Icon);
	String vlabName5Icon = GetterUtil.getString(portletPreferences.getValue("vlabName5-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName5Icon",vlabName5Icon);
	String vlabName6Icon = GetterUtil.getString(portletPreferences.getValue("vlabName6-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName6Icon",vlabName6Icon);
	String vlabName7Icon = GetterUtil.getString(portletPreferences.getValue("vlabName7-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName7Icon",vlabName7Icon);
	String vlabName8Icon = GetterUtil.getString(portletPreferences.getValue("vlabName8-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName8Icon",vlabName8Icon);
	String vlabName9Icon = GetterUtil.getString(portletPreferences.getValue("vlabName9-Icon", StringPool.BLANK));
	pageContext.setAttribute("vlabName9Icon",vlabName9Icon);
	
	

	pageContext.setAttribute("vlabName1Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName1-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName2Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName2-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName3Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName3-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName4Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName4-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName5Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName5-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName6Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName6-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName7Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName7-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName8Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName8-Title", StringPool.BLANK)));
	pageContext.setAttribute("vlabName9Title",
			GetterUtil.getString(portletPreferences.getValue("vlabName9-Title", StringPool.BLANK)));
	
	
	long vlabName1DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName1-DocumentId", StringPool.BLANK));
	long vlabName2DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName2-DocumentId", StringPool.BLANK));
	long vlabName3DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName3-DocumentId", StringPool.BLANK));
	long vlabName4DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName4-DocumentId", StringPool.BLANK));
	long vlabName5DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName5-DocumentId", StringPool.BLANK));
	long vlabName6DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName6-DocumentId", StringPool.BLANK));
	long vlabName7DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName7-DocumentId", StringPool.BLANK));
	long vlabName8DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName8-DocumentId", StringPool.BLANK));
	long vlabName9DocumentId = GetterUtil.getLong(portletPreferences.getValue("vlabName9-DocumentId", StringPool.BLANK));

	
	String url1 = GetterUtil.getString(portletPreferences.getValue("vlab1-Url", StringPool.BLANK));
	VLabBean vLabBean1 = Utils.getVLabBean(currentUser, url1);
	pageContext.setAttribute("vlab1Url", vLabBean1.getFriendlyURL());
	String ap1 = vLabBean1.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap1",ap1);

	String url2 = GetterUtil.getString(portletPreferences.getValue("vlab2-Url", StringPool.BLANK));	
	VLabBean vLabBean2 = Utils.getVLabBean(currentUser, url2);
	pageContext.setAttribute("vlab2Url", vLabBean2.getFriendlyURL());
	String ap2 = vLabBean2.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap2",ap2);
	
	String url3 = GetterUtil.getString(portletPreferences.getValue("vlab3-Url", StringPool.BLANK));	
	VLabBean vLabBean3 = Utils.getVLabBean(currentUser, url3);
	pageContext.setAttribute("vlab3Url", vLabBean3.getFriendlyURL());
	String ap3 = vLabBean3.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap3",ap3);
		
	String url4 = GetterUtil.getString(portletPreferences.getValue("vlab4-Url", StringPool.BLANK));	
	VLabBean vLabBean4 = Utils.getVLabBean(currentUser, url4);
	pageContext.setAttribute("vlab4Url", vLabBean4.getFriendlyURL());
	String ap4 = vLabBean4.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap4",ap4);
	
	String url5 = GetterUtil.getString(portletPreferences.getValue("vlab5-Url", StringPool.BLANK));
	VLabBean vLabBean5 = Utils.getVLabBean(currentUser, url5);
	pageContext.setAttribute("vlab5Url", vLabBean5.getFriendlyURL());
	String ap5 = vLabBean5.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap5",ap5);

	String url6 = GetterUtil.getString(portletPreferences.getValue("vlab6-Url", StringPool.BLANK));	
	VLabBean vLabBean6 = Utils.getVLabBean(currentUser, url6);
	pageContext.setAttribute("vlab6Url", vLabBean6.getFriendlyURL());
	String ap6 = vLabBean6.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap6",ap6);
	
	String url7 = GetterUtil.getString(portletPreferences.getValue("vlab7-Url", StringPool.BLANK));	
	VLabBean vLabBean7 = Utils.getVLabBean(currentUser, url7);
	pageContext.setAttribute("vlab7Url", vLabBean7.getFriendlyURL());
	String ap7 = vLabBean7.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap7",ap7);
	
	String url8 = GetterUtil.getString(portletPreferences.getValue("vlab8-Url", StringPool.BLANK));	
	VLabBean vLabBean8 = Utils.getVLabBean(currentUser, url8);
	pageContext.setAttribute("vlab8Url", vLabBean8.getFriendlyURL());
	String ap8 = vLabBean8.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap8",ap8);
	
	String url9 = GetterUtil.getString(portletPreferences.getValue("vlab9-Url", StringPool.BLANK));	
	VLabBean vLabBean9 = Utils.getVLabBean(currentUser, url9);
	pageContext.setAttribute("vlab9Url", vLabBean9.getFriendlyURL());
	String ap9 = vLabBean9.getAccessPolicy().toString().toLowerCase();
	pageContext.setAttribute("ap9",ap9);

	String content1 = "";
	if (vlabName1DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName1DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content1 = node.getText();
	}

	String content2 = "";
	if (vlabName2DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName2DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content2 = node.getText();
	}

	String content3 = "";
	if (vlabName3DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName3DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content3 = node.getText();
	}
	
	String content4 = "";
	if (vlabName4DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName4DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content4 = node.getText();
	}
	
	String content5 = "";
	if (vlabName5DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName5DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content5 = node.getText();
	}
	String content6 = "";
	if (vlabName6DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName6DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content6 = node.getText();
	}
	String content7 = "";
	if (vlabName7DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName7DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content7 = node.getText();
	}
	String content8 = "";
	if (vlabName8DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName8DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content8 = node.getText();
	}
	String content9 = "";
	if (vlabName9DocumentId > 0) {
		JournalArticle article = JournalArticleLocalServiceUtil.getArticle(groupId,
				"" + vlabName9DocumentId);
		Document document = SAXReaderUtil.read(article.getContent());
		Node node = document.selectSingleNode("/root/static-content");
		content9 = node.getText();
	}
	
	 Random rand = new Random();
	 final int MAX_NUMBER = 999999; //this is needed for generation of random div identifier
%>
<c:if test="${not empty vlabName1Title}">
 <% 
 	String assetAppSummaryPre1 = "assetAppSummaryPre1" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary1 = "assetAppSummary1" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre1",assetAppSummaryPre1);
	pageContext.setAttribute("assetAppSummary1",assetAppSummary1);
	
	String pickedIcon1URL = icondocURL;
	pageContext.setAttribute("pickedIcon1URL",pickedIcon1URL);
	if (vlabName1Icon != null && vlabName1Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon1URL",vlabName1Icon);
		
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab1Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon1URL}">
				${vlabName1Title}</a><span
			class="access-policy ${ap1}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap1}.png" />&nbsp;&nbsp;${ap1}</span>
	</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre1}"> <%
 	if (content1.length() > abstractCharsLenghtMax)
 			out.println(HtmlUtil.stripHtml(content1.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#${assetAppSummaryPre1}', '#${assetAppSummary1}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary1}" class="asset-summary"
						style="display: none;"><%=content1%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<c:if test="${not empty vlabName2Title}">
<!--  SECOND  -->
 <% 
 	String assetAppSummaryPre2 = "assetAppSummaryPre2" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary2 = "assetAppSummary2" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre2",assetAppSummaryPre2);
	pageContext.setAttribute("assetAppSummary2",assetAppSummary2);
	
	String pickedIcon2URL = icondocURL;
	pageContext.setAttribute("pickedIcon2URL",pickedIcon2URL);
	if (vlabName2Icon != null && vlabName2Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon2URL",vlabName2Icon);
 %>
<div class="asset-abstract">
	<h3 class="asset-title">
		<a href="${vlab2Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon2URL}">
			${vlabName2Title}</a><span
			class="access-policy ${ap2}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap2}.png" />&nbsp;&nbsp;${ap2}</span>
	</h3>
	<div class="asset-content">
		<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre2}"> <%
 	if (content2.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content2.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
				<div>
					<a
						href="javascript:switchView('#${assetAppSummaryPre2}', '#${assetAppSummary2}');">Read
						More � </a>
				</div>
			</div>
			<div class="asset-more">
				<div id="${assetAppSummary2}" class="asset-summary"
					style="display: none;"><%=content2%></div>
			</div>
		</div>
		<div class="asset-metadata"></div>
	</div>
</div>
</c:if>
<c:if test="${not empty vlabName3Title}">
	<!--  THIRD  -->
	 <% 
 	String assetAppSummaryPre3 = "assetAppSummaryPre3" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary3 = "assetAppSummary3" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre3",assetAppSummaryPre3);
	pageContext.setAttribute("assetAppSummary3",assetAppSummary3);
	
	String pickedIcon3URL = icondocURL;
	pageContext.setAttribute("pickedIcon3URL",pickedIcon3URL);
	if (vlabName3Icon != null && vlabName3Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon3URL",vlabName3Icon);
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab3Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon3URL}">
				${vlabName3Title}</a><span
			class="access-policy ${ap3}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap3}.png" />&nbsp;&nbsp;${ap3}</span>
	</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre3}"> <%
 	if (content3.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content3.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
						href="javascript:switchView('#${assetAppSummaryPre3}', '#${assetAppSummary3}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary3}" class="asset-summary"
						style="display: none;"><%=content3%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>
<c:if test="${not empty vlabName4Title}">
	<!--  FOURTH  -->
		 <% 
 	String assetAppSummaryPre4 = "assetAppSummaryPre4" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary4 = "assetAppSummary4" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre4",assetAppSummaryPre4);
	pageContext.setAttribute("assetAppSummary4",assetAppSummary4);
	
	
	String pickedIcon4URL = icondocURL;
	pageContext.setAttribute("pickedIcon4URL",pickedIcon4URL);
	if (vlabName4Icon != null && vlabName4Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon4URL",vlabName4Icon);
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab4Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon4URL}">
				${vlabName4Title}</a><span
			class="access-policy ${ap4}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap4}.png" />&nbsp;&nbsp;${ap4}</span>
	</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre4}"> <%
 	if (content4.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content4.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
						href="javascript:switchView('#${assetAppSummaryPre4}', '#${assetAppSummary4}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary4}" class="asset-summary"
						style="display: none;"><%=content4%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>

<c:if test="${not empty vlabName5Title}">
	<!--  FIFTH  -->
		 <% 
 	String assetAppSummaryPre5 = "assetAppSummaryPre5" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary5 = "assetAppSummary5" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre5",assetAppSummaryPre5);
	pageContext.setAttribute("assetAppSummary5",assetAppSummary5);
	
	String pickedIcon5URL = icondocURL;
	pageContext.setAttribute("pickedIcon5URL",pickedIcon5URL);
	if (vlabName5Icon != null && vlabName5Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon5URL",vlabName5Icon);
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab5Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon5URL}">
				${vlabName5Title}</a><span
			class="access-policy ${ap5}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap5}.png" />&nbsp;&nbsp;${ap5}</span>
	</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre5}"> <%
 	if (content5.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content5.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
						href="javascript:switchView('#${assetAppSummaryPre5}', '#${assetAppSummary5}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary5}" class="asset-summary"
						style="display: none;"><%=content5%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>

<c:if test="${not empty vlabName6Title}">
	<!--  SIXTH  -->
			 <% 
 	String assetAppSummaryPre6 = "assetAppSummaryPre6" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary6 = "assetAppSummary6" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre6",assetAppSummaryPre6);
	pageContext.setAttribute("assetAppSummary6",assetAppSummary6);
	
	String pickedIcon6URL = icondocURL;
	pageContext.setAttribute("pickedIcon6URL",pickedIcon6URL);
	if (vlabName6Icon != null && vlabName6Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon6URL",vlabName6Icon);
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab6Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon6URL}">
				${vlabName6Title}</a><span
			class="access-policy ${ap6}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap6}.png" />&nbsp;&nbsp;${ap6}</span>
	</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre6}"> <%
 	if (content6.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content6.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#${assetAppSummaryPre6}', '#${assetAppSummary6}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary6}" class="asset-summary"
						style="display: none;"><%=content6%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>

<c:if test="${not empty vlabName7Title}">
	<!--  SEVENTH  -->
				 <% 
 	String assetAppSummaryPre7 = "assetAppSummaryPre7" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary7 = "assetAppSummary7" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre7",assetAppSummaryPre7);
	pageContext.setAttribute("assetAppSummary7",assetAppSummary7);
	
	String pickedIcon7URL = icondocURL;
	pageContext.setAttribute("pickedIcon7URL",pickedIcon7URL);
	if (vlabName7Icon != null && vlabName7Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon7URL",vlabName7Icon);
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab7Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon7URL}">
				${vlabName7Title}</a><span
			class="access-policy ${ap7}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap7}.png" />&nbsp;&nbsp;${ap7}</span>
	</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre7}"> <%
 	if (content7.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content7.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#${assetAppSummaryPre7}', '#${assetAppSummary7}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary7}" class="asset-summary"
						style="display: none;"><%=content7%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>


<c:if test="${not empty vlabName8Title}">
	<!--  8  -->
					 <% 
 	String assetAppSummaryPre8 = "assetAppSummaryPre8" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary8 = "assetAppSummary8" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre8",assetAppSummaryPre8);
	pageContext.setAttribute("assetAppSummary8",assetAppSummary8);
	
	String pickedIcon8URL = icondocURL;
	pageContext.setAttribute("pickedIcon8URL",pickedIcon8URL);
	if (vlabName8Icon != null && vlabName8Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon8URL",vlabName8Icon);
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab8Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon8URL}">
				${vlabName8Title}</a><span
			class="access-policy ${ap8}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap8}.png" />&nbsp;&nbsp;${ap8}</span>
	</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre8}"> <%
 	if (content8.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content8.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#${assetAppSummaryPre8}', '#${assetAppSummary8}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary8}" class="asset-summary"
						style="display: none;"><%=content8%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>


<c:if test="${not empty vlabName9Title}">
	<!--  NINTH  -->
						 <% 
 	String assetAppSummaryPre9 = "assetAppSummaryPre9" + rand.nextInt(MAX_NUMBER);
	String assetAppSummary9 = "assetAppSummary9" + rand.nextInt(MAX_NUMBER);
	pageContext.setAttribute("assetAppSummaryPre9",assetAppSummaryPre9);
	pageContext.setAttribute("assetAppSummary9",assetAppSummary9);
	
	String pickedIcon9URL = icondocURL;
	pageContext.setAttribute("pickedIcon9URL",pickedIcon9URL);
	if (vlabName9Icon != null && vlabName9Icon.compareTo(StringPool.BLANK) != 0)
		pageContext.setAttribute("pickedIcon9URL",vlabName9Icon);
 %>
	<div class="asset-abstract">
		<h3 class="asset-title">
			<a href="${vlab9Url}"><img alt="" style="width: ${icondocURLWidthPx}px; margin: 5px;" src="${pickedIcon9URL}">
				${vlabName9Title}</a><span
			class="access-policy ${ap9}-color"><img 
			src="<%=renderRequest.getContextPath()%>/images/${ap9}.png" />&nbsp;&nbsp;${ap9}</span>
		</h3>
		<div class="asset-content">
			<div class="asset-summary" style="margin-left: 25px;">
				<div id="${assetAppSummaryPre9}"> <%
 	if (content9.length() > abstractCharsLenghtMax)
 		out.println(HtmlUtil.stripHtml(content9.substring(0, abstractCharsLenghtMax) + " ..."));
 %>
					<div>
						<a
							href="javascript:switchView('#${assetAppSummaryPre9}', '#${assetAppSummary9}');">Read
							More � </a>
					</div>
				</div>
				<div class="asset-more">
					<div id="${assetAppSummary9}" class="asset-summary"
						style="display: none;"><%=content9%></div>
				</div>
			</div>
			<div class="asset-metadata"></div>
		</div>
	</div>
</c:if>